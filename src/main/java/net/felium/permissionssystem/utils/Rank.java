package net.felium.permissionssystem.utils;

import java.util.List;

public class Rank {

    private String order;
    private String prefix;
    private String separator;
    private List<String> permissions;

    public Rank(String order, String prefix, String separator, List<String> permissions) {
        this.order = order;
        this.prefix = prefix;
        this.separator = separator;
        this.permissions = permissions;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSeparator() {
        return separator;
    }

    public void setSeparator(String separator) {
        this.separator = separator;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }
}
