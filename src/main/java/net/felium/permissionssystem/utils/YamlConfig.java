package net.felium.permissionssystem.utils;

import org.bukkit.ChatColor;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;

public class YamlConfig extends YamlConfiguration {

    private String name;
    private JavaPlugin plugin;

    private File file;

    public YamlConfig(String name, JavaPlugin plugin, boolean load) {
        this.name = name;
        this.plugin = plugin;

        if (load)
            load();
    }

    public YamlConfig(String name, JavaPlugin plugin) {
        this(name, plugin, true);
    }

    public void load() {
        if (!plugin.getDataFolder().exists())
            if (!plugin.getDataFolder().mkdirs())
                throw new RuntimeException("Could not create Config: " + name);

        file = new File(plugin.getDataFolder(), name);
        try {
            if (!file.exists())
                if (!file.createNewFile())
                    throw new RuntimeException("Could not create Config: " + name);
            load(file);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void save(boolean load) {
        try {
            save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (load)
            load();
    }

    public void save() {
        save(false);
    }

    public void def(String path, Object value) {
        if (isSet(path))
            return;
        super.set(path, value);
    }

    public String getTranslatedString(String path) {
        return ChatColor.translateAlternateColorCodes('&', getString(path)).replaceAll("§", "\u00A7");
    }
}
