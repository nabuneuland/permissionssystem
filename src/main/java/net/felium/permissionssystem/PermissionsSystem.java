package net.felium.permissionssystem;

import net.felium.permissionssystem.utils.CommandSign;
import net.felium.permissionssystem.utils.Rank;
import net.felium.permissionssystem.utils.YamlConfig;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Scoreboard;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PermissionsSystem extends JavaPlugin implements Listener {

    private YamlConfig players;
    private YamlConfig ranks;

    private String defaultRank;
    private Map<String, Rank> rankMap;
    private Scoreboard scoreboard;

    @Override
    public void onEnable() {
        players = new YamlConfig("players.yml", this);
        ranks = new YamlConfig("ranks.yml", this);
        scoreboard = Bukkit.getScoreboardManager().getNewScoreboard();

        Bukkit.getPluginManager().registerEvents(this, this);

        createDefaultConfig();
        readConfigs();
        updateForAll();

        // TODO: 04.05.2019 delete this line
        Bukkit.getPluginManager().registerEvents(CommandSign.getInstance(), this);
    }

    private void updateForAll() {
        Bukkit.getOnlinePlayers().forEach(player -> {
            String rank = players.getString(player.getUniqueId().toString());
            if (rank == null) {
                rank = defaultRank;
                players.set(player.getUniqueId().toString(), rank);
                players.save();
            }

            scoreboard.getTeam(rankMap.get(rank).getOrder() + rank).addEntry(player.getName());
            player.setDisplayName(rankMap.get(rank).getPrefix() + player.getName());
        });

        Bukkit.getOnlinePlayers().forEach(players -> players.setScoreboard(scoreboard));
    }

    private void readConfigs() {
        defaultRank = ranks.getString("default");
        rankMap = new HashMap<>();

        ranks.getConfigurationSection("ranks").getKeys(false).forEach(rank -> {
            rankMap.put(rank, new Rank(
                    ranks.getString("ranks." + rank + ".order"),
                    ranks.getTranslatedString("ranks." + rank + ".prefix"),
                    ranks.getTranslatedString("ranks." + rank + ".separator"),
                    ranks.getStringList("ranks." + rank + ".permissions")
            ));

            String rankName = rankMap.get(rank).getOrder() + rank;
            scoreboard.registerNewTeam(rankName);
            scoreboard.getTeam(rankName).setPrefix(rankMap.get(rank).getPrefix());
        });
    }

    private void createDefaultConfig() {
        ranks.def("ranks.player.order", "2");
        ranks.def("ranks.player.prefix", "Player | ");
        ranks.def("ranks.player.separator", "&6>>&3");
        ranks.def("ranks.player.permissions", Collections.emptyList());

        ranks.def("ranks.admin.order", "0");
        ranks.def("ranks.admin.prefix", "Admin | ");
        ranks.def("ranks.admin.separator", "&3>>&4");
        ranks.def("ranks.admin.permissions", Collections.emptyList());

        ranks.def("default", "player");

        ranks.save(true);
    }

    private Rank getRank(UUID uuid) {
        String rank = players.getString(uuid.toString());
        if (rank == null) {
            rank = defaultRank;
            players.set(uuid.toString(), rank);
            players.save();
        }

        return rankMap.get(rank);
    }

    private void addPermissions(Player player, Rank rank) {
        PermissionAttachment attachment = player.addAttachment(this);

        rank.getPermissions().forEach(permission -> attachment.setPermission(permission, true));

        updateForAll();
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent event) {
        addPermissions(event.getPlayer(), getRank(event.getPlayer().getUniqueId()));
    }

    @EventHandler
    public void onAsyncPlayerChatEvent(AsyncPlayerChatEvent event) {
        Rank rank = getRank(event.getPlayer().getUniqueId());

        event.setFormat(rank.getPrefix() + event.getPlayer().getName() + ' ' + rank.getSeparator() + ' ' + event.getMessage());
    }

    // DELETE BELOW THIS
    @EventHandler
    public void onBlockPlaceEvent(BlockPlaceEvent event) {
        if (event.getBlock().getType().equals(Material.BEDROCK)) {
            Sign sign = CommandSign.getInstance().createSign(new Location(Bukkit.getWorld("world"), 1, 2, 3), (player, location) -> {

            });

            Sign wallSign = CommandSign.getInstance().createWallSign(new Location(Bukkit.getWorld("world"), 4, 5, 6), (player, location) -> {

            });
        }
        System.out.println("dafs");
    }
}
